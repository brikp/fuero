﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

    /// <summary>
    /// State manager (only used for pausing). Pause/unpause game by pressing space.
    /// </summary>

    public Text refText;

    private bool pauseState;
    private string text;

	// Use this for initialization
	void Start () {
        pauseState = false;
        text = (pauseState) ? "Paused" : "Unpaused";
        text += "\nPress Space to pause/unpause.";
        refText.text = text;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("space")) {

            pauseState = !pauseState;
            string function = "";

            if (!pauseState) {
                function = "OnResume";
                Time.timeScale = 1.0f;
            } else {
                function = "OnPause";
                Time.timeScale = 0.0f;
            }

            Object[] objects = FindObjectsOfType(typeof(GameObject));
            foreach (GameObject go in objects) {
                go.SendMessage(function, SendMessageOptions.DontRequireReceiver);
            }

            text = (pauseState) ? "Paused" : "Unpaused";
            text += "\nPress Space to pause/unpause.";
            refText.text = text;
        }
    }
}
