﻿using UnityEngine;
using System.Collections;

public class TestClass : MonoBehaviour {

    /// <summary>
    /// Test class for the generator.
    /// </summary>

    private string str1;
    private int int1;

    void Start() {
        str1 = "";
        int1 = 0;
    }

    void Update() {

    }

    public void Init(string s, int i) {
        str1 = s;
        int1 = i;
        Debug.Log("Invoking TestClass!; Params: " + s + ", " + i);
    }

}
