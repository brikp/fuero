﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OreMine : MonoBehaviour {

    /// <summary>
    /// Incremental resource spawner.
    /// </summary>

    public float spawnRate; // resource/hr
    public Text refText;

    private float increment;
    private bool paused;
    private float counter;
    private float buffered;
    private float pausedDeltaTime;
    private float pausedFrameStartTime;

    // Use this for initialization
    void Start() {
        increment = spawnRate / 3600; // dodawane/sek
        counter = 0;
        paused = false;
        buffered = 0;
        pausedDeltaTime = 0;
        pausedFrameStartTime = 0;
        refText.text = "Mined: " + counter + "; Buffered: " + buffered + ";";
    }

    // Update is called once per frame
    void Update() {

        if (!paused) {
            counter += increment * Time.deltaTime;
        } else {
            pausedDeltaTime = Time.realtimeSinceStartup - pausedFrameStartTime;
            pausedFrameStartTime = Time.realtimeSinceStartup;

            buffered += increment * pausedDeltaTime;
        }
        refText.text = "Mined: " + counter + "; Buffered: " + buffered + ";";
    }

    void OnPause() {
        paused = true;
        pausedFrameStartTime = Time.realtimeSinceStartup;
    }

    void OnResume() {
        paused = false;
        counter += buffered;
        buffered = 0;
    }
}
