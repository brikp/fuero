﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UnpauserTimer : MonoBehaviour {

    /// <summary>
    /// Unpaused time since startup.
    /// </summary>

    private Text refText;

	// Use this for initialization
	void Start () {
        refText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        refText.text = "Unpaused: " + Time.time;
	}
}
