﻿using UnityEngine;

public class Generator : MonoBehaviour {

    /// <summary>
    /// Universal generator class. Generated classes need an Init() function that assigns the data to parameters.
    /// Random value ranges and class to be generated (input name) can be changed through Unity Inspector.
    /// "g" key can be used to generate additional objects.
    /// </summary>

    public string classToGenerate = "TestClass";
    public float minValue = 0;
    public float maxValue = 100;
    public int minStrLength = 1;
    public int maxStrLength = 10;

	// Use this for initialization
	void Start () {
        generate(System.Type.GetType(classToGenerate));
	}

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown("g")) {
            generate(System.Type.GetType(classToGenerate));
        }
    }

    void OnValidate() {
        checkParameters();
    }

    int generateInt() {
        return (int)Random.Range(minValue, maxValue);
    }

    float generateFloat() {
        return Random.Range(minValue, maxValue);
    }

    string generateString() {
        string result = "";
        int length = (int)Random.Range(minStrLength, maxStrLength);
        string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (int i = 0; i < length; i++) {
            result += chars[(int)Random.Range(0,chars.Length - 1)];
        }
        //Debug.Log(result);
        return result;
    }

    void generate(System.Type type) {
        System.Reflection.MethodInfo init = type.GetMethod("Init");

        System.Reflection.ParameterInfo[] parameters = init.GetParameters();
        object[] newParameters = new object[parameters.Length];
        //Debug.Log("Parameters: " + newParameters.Length);

        for (int i = 0; i < parameters.Length; i++) {
            if (parameters[i].ParameterType.Equals(typeof(string))) {
                newParameters[i] = generateString();
                //Debug.Log(i + " string" + newParameters[i]);
            } else if (parameters[i].ParameterType.Equals(typeof(int))) {
                newParameters[i] = generateInt();
                //Debug.Log(i + " int " + newParameters[i]);
            } else if (parameters[i].ParameterType.Equals(typeof(float)) || parameters[i].ParameterType.Equals(typeof(double))) {
                newParameters[i] = generateFloat();
                //Debug.Log(i + " float" + newParameters[i]);
            } else
                newParameters[i] = null;
        }

        GameObject obj = new GameObject();
        Component comp = obj.AddComponent(type);
        init.Invoke(comp, newParameters);
    }

    void checkParameters() {
        minValue = (minValue > maxValue) ? maxValue : minValue;
        maxValue = (maxValue < minValue) ? minValue : maxValue;
        minStrLength = (minStrLength < 0) ? 0 : minStrLength;
        maxStrLength = (maxStrLength < 0) ? 0 : maxStrLength;
        minStrLength = (minStrLength > maxStrLength) ? maxStrLength : minStrLength;
        maxStrLength = (maxStrLength < minStrLength) ? minStrLength : maxStrLength;
    }
    
}
