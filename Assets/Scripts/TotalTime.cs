﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TotalTime : MonoBehaviour {

    /// <summary>
    /// Total time since startup (including paused time).
    /// </summary>

    private Text refText;

    // Use this for initialization
    void Start() {
        refText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        refText.text = "Total: " + (Time.realtimeSinceStartup - 1);
    }
}
