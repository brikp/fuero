﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Spawner : MonoBehaviour {

    /// <summary>
    /// Object spawner. Buffers objects to be created and spawns them only when game is unpaused.
    /// </summary>

    public float spawnRate = 3600; // objects/hr
    public GameObject spawnedObject;
    public Text refText;

    private float spawnIncrement;
    private float timeToSpawn;
    private float timeToBuffer;
    private bool paused;
    private int buffered;
    private float bufferedSpawnRate;
    private float pausedDeltaTime;
    private float pausedFrameStartTime;
    private int counter;

    // Use this for initialization
    void Start() {
        spawnIncrement = 3600 / spawnRate;
        bufferedSpawnRate = spawnIncrement / 3;
        timeToBuffer = spawnIncrement;
        timeToSpawn = (buffered > 0)?bufferedSpawnRate:spawnIncrement;
        counter = 0;
        paused = false;
        buffered = 0;
        pausedDeltaTime = 0;
        pausedFrameStartTime = 0;
        refText.text = "Spawned: " + counter + "; Buffered: " + buffered + ";";
    }

    // Update is called once per frame
    void Update() {

        if (!paused) {
            timeToBuffer -= Time.deltaTime;
            timeToSpawn -= Time.deltaTime;
        } else {
            pausedDeltaTime = Time.realtimeSinceStartup - pausedFrameStartTime;
            pausedFrameStartTime = Time.realtimeSinceStartup;
            timeToBuffer -= pausedDeltaTime;
        }

        if (timeToBuffer <= 0) {
            buffered += 1;
            timeToBuffer = spawnIncrement;
        }

        if (timeToSpawn <= 0) {
            counter += 1;
            Instantiate(spawnedObject, transform.position, transform.rotation);
            buffered -= 1;
            timeToSpawn = (buffered > 0) ? bufferedSpawnRate : spawnIncrement;
        }
        refText.text = "Spawned: " + counter + "; Buffered: " + buffered + ";";
    }

    void OnPause() {
        paused = true;
        pausedFrameStartTime = Time.realtimeSinceStartup;
    }

    void OnResume() {
        paused = false;
    }
}