﻿using UnityEngine;
using System.Collections;

public class TestClass2 : MonoBehaviour {

    /// <summary>
    /// Test class for the generator.
    /// </summary>

    private string str1;
    private int int1;
    private string str2;
    private int int2;

    void Start() {
        str1 = "";
        str2 = "";
        int1 = 0;
        int2 = 0;
    }

    void Update() {

    }

    public void Init(string s, int i, string s2, int i2) {
        str1 = s;
        int1 = i;
        str2 = s2;
        int2 = i2;
        Debug.Log("Invoking TestClass2!; Params: " + s + ", " + i + ", " + s2 + ", " + i2);
    }

}
